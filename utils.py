import json
import os
import pickle
from collections import defaultdict
from model_calls import ner_inference
from bs4 import BeautifulSoup
import re
'''
This file contains utility functions for the project.
'''


def dita_to_json(root, output_folder):
    '''
    This function takes in a folder containing DITA/xml files and converts them to json.
    args:
        root(str): the path to the folder containing the DITA files
        output_folder(str): the path to the folder to output the json files
    '''
    dirs = []

    # find all .ditamap files, which represent the root of documents

    for root, dirnames, filenames in os.walk(root):
        l = [f for f in filenames if ".ditamap" in f]
        if len(l) > 0:
            dirs.append((root,l[0]))

    def load_section(sec,r=False):
        children = sec.find_all(recursive=r)
        counts = len(children)
        for child in children:
            #if r:
            #    print(child)
            name = child.name
            if name == "p":
                yield {
                    "type": "paragraph",
                    "content": " ".join(filter(None,child.text.replace("\n","").split(" "))).strip()#list(load_section(child))
                }
            elif name == "xref" and child.get("href") is not None:
                yield {
                    "type": "xref",
                    "content": child.text,
                    "href": child["href"]
                }
            elif name == "fig":
                try:
                    yield {
                        "type": "image",
                        "href": child("image")[0]["href"]
                    }
                except:
                    pass
            elif name == "image":
                yield {
                    "type": "image",
                    "href": child["href"]
                }
            elif name == "table":
                yield {
                    "type": "table",
                    "content": [[list(load_section(cell,True)) for cell in row("entry")] for row in child("row")]
                }
            elif name == "simpletable":
                yield {
                    "type": "table",
                    "content": [[list(load_section(cell,True)) for cell in row("stentry")] for row in child("strow")]
                }
            elif name == "ul":
                yield {
                    "type": "list",
                    "content": [list(load_section(c)) for c in child("li")]
                }
            elif name == "section":
                yield {
                    "type": "section",
                    "title": child("title")[0].text,
                    "content": list(load_section(child))
                }
            else:
                counts -= 1
                
        if counts == 0:
            yield {"type":"paragraph","content":sec.text}

    def load_dita(file):
        body = BeautifulSoup("\n".join(open("{}/{}".format(d,file)).readlines()))("conbody")
            
        return [] if len(body) == 0 else list(load_section(body[0]))

    # load topics
    def load_topic(topic):
        title = None
        try:
            title = topic("topicmeta")[0]("navtitle")[0].text
        except:
            pass
        try:
            return {
                "title": title,
                "sections": load_dita(topic["href"]),
                "href": topic["href"]
            }
        except Exception as e:
            print(e)
            return {}

    for d,f in dirs:
        if ".ipynb" in d:
            continue

        doc = BeautifulSoup("\n".join(open("{}/{}".format(d,f)).readlines()))

        parts = [load_topic(notice) for notice in doc("notices")] + [load_topic(topic) for part in doc("part") for topic in part("topicref")] 

        # title and other metadata
        doc = {
            "booktype": doc("booklibrary")[0].text,
            "title": doc("mainbooktitle")[0].text,
            "subtitle": "\n".join(titles.text for titles in doc("booktitlealt")),
            "desc": doc("shortdesc")[0].text,
            "parts":parts
        }

        # save parsed elements to json
        with open("{}/{}.json".format(output_folder,f),"w+") as out:
            json.dump(doc,out)

def json_to_list_rulebooks(input_folder='data/modules_json', book_type='handbook',output='rulebook_list', save=False):
    '''
    This function takes in a folder containing json files and converts them to a list of rulebooks 
    items.
    args:
        input_folder(str): the path to the folder containing the json files
        book_type(str): the type of book to be processed, either 'handbook' or 'module'
        output(str): the path to the folder to output the rulebook list
        save(bool): whether to save the list to a pickle file
    returns:
        sent_list(list): a list of rulebook items
    '''
    books = os.listdir(input_folder)
    sent_list = []
    for book in books:
        with open(f'{input_folder}/{book}') as file:
            dic = json.load(file)
        if book_type == 'module':
            name = book.split('_')[2].split('.')[0]
            book_name = name + '£ ' + dic['title'] + '£'
        else:
            book_name = dic['title'] + '£ ' + dic['subtitle'][:-1] + '£'
        for part in dic['parts']:
            if part == {}:
                continue

            if part['title'] is not None:
                title = part['title'] + '£'
            title = str(part['title']) + '£'              
            for i,section in enumerate(part['sections']):
                if section['type'] == 'paragraph':
                    sent = f"{title} " + section['content']

                if section['type'] == 'list':
                    l_str = ' '
                    context = sent_list.pop()
                    for item in section['content']:
                        l_str += f"{item[0]['content']}, "
                    sent = ' '.join((context + l_str).split())
                    sent_list.append(sent)
                    continue

                if section['type'] != 'section':
                    sent_list.append(f"{book_name} {sent}")

                if section['type'] == 'section':
                    for j,sub_section in enumerate(section['content']):    
                        if sub_section['type'] == 'paragraph':
                            sent = f"{title}. " + sub_section['content']
                            new_item = f"{book_name} {title} {section['title']} - {sub_section['content']}"
                            sent_list.append(new_item)
                            continue
                        if sub_section['type'] == 'list':
                            l_str = ''
                            context = sent_list.pop()
                            for item in sub_section['content']:
                                l_str += f"{item[0]['content']}, "
                            
                            sent = context + l_str
                        sent_list.append(f"{sent}")

    if save:
        pickle.dump(sent_list, open(f"{output}.pt",'wb'))
    return sent_list


        

def full_sents_to_parts(sent_list, save=False, output='parts.pt'):
    '''
    This function takes the outputs of json_to_list_rulebooks and splits the 
    items into a dictionary with each the sentence, section, subtitle and rulebook name.
    args:
        sent_list(list): the list of sentences from json_to_list_rulebooks
        save(bool): whether to save the list to a pickle file
        output(str): the path to the folder to output the rulebook list
    returns:
        parts(list): a list of dictionaries with the following keys:
            sentence(str): the sentence
            section(str): the section
            subtitle(str): the subtitle
            rulebook(str): the rulebook name
    '''
    out = []
    for sent in sent_list:
        temp_sent = sent.replace('\t','')
        sent = ' '.join(temp_sent.split())
        x = sent.split('£')
        n = len(x)
        if n != 4:
            continue
        title = x[0].strip()
        sub = x[1].strip()
        sec = x[2].strip()
        s = x[3].strip()
        out.append({'title':title, 'sub_title':sub, 'section':sec, 'sentence':s})
    if save:
        pickle.dump(out, open(output,'wb'))
    return out

def co_ref_sentences(data_pth='data/handbooks.pt', model_pth='models/ner_model'):
    '''
    This function loads sentence from the handbooks and co-references them with
    the people nodes from the NER results.
    args:
        data_pth(str): the path to the handbooks pickle file
        model_pth(str): the path to the NER model
    returns:
        sents(dict): a dictionary with the person as the key and the a list of 
        co referenced sentences as the value
    '''
    handbooks = pickle.load(open(data_pth, 'rb'))
    handbook_data = full_sents_to_parts(handbooks)
    sents = defaultdict(list)
    titles = []
    data_sents = []
    for data in handbook_data:
        title = f"{data['title']} {data['sub_title']} ".replace('\n',' ') + data['section']
        title = remove_punctuation(title)
        titles.append(title)
        data_sents.append(data['sentence'])
    
    res = ner_inference(titles, model_pth)
    for i, title in enumerate(titles):
        people = []
        for x in res[title]:
            if x[1]=='People':
                people.append(x[0])
        new_you = ', '.join(people)
        if 'you ' in data_sents[i] or 'You ' in data_sents[i]:
            sent = data_sents[i].replace('you', new_you)
            sent = sent.replace('You', new_you)
        else:
            sent = data_sents[i]
        for p in people:
            if sent:
                temp_sent = sent.replace('   ',' ')
                sents[p].append((temp_sent, title))

    return sents

def remove_punctuation(text):
    '''
    This function removes punctuation from a string.
    args:
        text(str): the string to remove punctuation from
    returns:   
        text(str): the string with punctuation removed
    '''
    return re.sub(r"[;@#?!&$-()’]+\ *", " ", text).replace('  ',' ').strip()
