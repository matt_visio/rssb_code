from transformers import AutoTokenizer, AutoModelForTokenClassification, DataCollatorForTokenClassification, Trainer, TrainingArguments
from transformers import pipeline
from sentence_transformers import util
from spacy import displacy
from spacy.matcher import Matcher
from datasets import load_dataset,DatasetDict, load_metric
import numpy as np
import torch
import re

def ner_inference(test, model_pth='models/ner_model'):
    '''
    Function to predict the NER tags for a given list of sentences

    Args:
        test (list): List of sentences
        model_pth (str): Path to the model
    Returns:
        nodes (dict): Dictinoary with sentence as key and list of tuples as value (token, tag) 
    '''
    abbreviations = {'individual working alone':'iwa', 'engineering supervisor':'es', 'safe work leader':'swl',
        'person in charge of the siding possession':'picos', 'person in charge of the possession':'picop',
        'controller of site safety':'coss','machine controller':'mc', 'protection controller':'pc'}
    nodes = {}
    tokenizer = AutoTokenizer.from_pretrained(model_pth)
    model = AutoModelForTokenClassification.from_pretrained(model_pth)
    model.config.id2label = {0:'None',1:'Object',2:'Event',3:'Location',4:'Action',5:'People',6:'Other'}
    if torch.cuda.is_available():
        nlp = pipeline('ner', model=model, tokenizer=tokenizer, aggregation_strategy="simple", device=0)
    else:
        nlp = pipeline('ner', model=model, tokenizer=tokenizer, aggregation_strategy="simple")
    res = nlp(test)

    for i,sent in enumerate(res):
        pred = ''
        nodes[test[i]] = []
        for j, guess in enumerate(sent):
            item = guess['word']
            item =  re.sub(r"[;@#?!&$-()’]+\ *", " ", item).replace('  ',' ').strip()
            ent = guess['entity_group']
            for ab in abbreviations.keys():
                if ab in item and abbreviations[ab]:
                    item = item.replace(ab,'')
            if '##' in item:
                pred = pred[:] + f"{item.replace('##','')} "
                tag = nodes[test[i]][-1][1]
                nodes[test[i]].pop()
            else:
                pred = f"{item}"
                tag = ent
            if pred:
                nodes[test[i]].append((pred.strip(),tag))
    return nodes

def qa_inference(embedder, corpus_embeddings,text, cross_encoder, qa_pipeline, qs, n=3, books=None, show=False):
    '''
    Function to predict the QA tags for a given list of sentences

    Args:
        embedder (SentenceTransformer or Hugging Face file): Sentence embedder
        corpus_embeddings (list): list of embeddings for each sentence
        text (list): List of sentences
        cross_encoder (CrossEncoder): CrossEncoder object
        qa_pipeline (Pipeline): QA pipeline
        qs (list): List of questions
        n (int): Number of top questions to return
        books (dict): Dictionary with sentence as key and list of rulebook as value
        show (bool): Whether to show the results or not
    Returns:
        nodes (dict): Dictionary with sentence as key and list of tuples as value (token, tag) 
    '''
       #Truncate long passages to 256 tokens  
    def search(query, n = 3,top_k = 32, show = True, bi_encoder = embedder, passages = text, corpus_embeddings = corpus_embeddings):
        '''
        Function to search find k nearest passages to the question using sematnic search
        then return the top n passages based on the cross-encoder score.

        Args:
            query (str): Question
            n (int): Number of passages to return
            top_k (int): Number of passages to get from sematnic search
            show (bool): Whether to show the results or not
            bi_encoder (SentenceTransformer): Sentence embedder
            passages (list): List of passages
            corpus_embeddings (list): List of embeddings for each passage
        Returns:
            passages (list): List of passages
            corpus_embeddings (list): List of embeddings for each passage
        '''
        bi_encoder.max_seq_length = 256 
        question_embedding = bi_encoder.encode(query, convert_to_tensor=True)
        if torch.cuda.is_available():
            question_embedding = question_embedding.cuda()
        else:
            question_embedding = question_embedding
        hits = util.semantic_search(question_embedding, corpus_embeddings, top_k=top_k)
        hits = hits[0]  # Get the hits for the first query

        cross_inp = [[query, passages[hit['corpus_id']]] for hit in hits]
        cross_scores = cross_encoder.predict(cross_inp)

        for idx in range(len(cross_scores)):
            hits[idx]['cross-score'] = cross_scores[idx]

        hits = sorted(hits, key=lambda x: x['cross-score'], reverse=True)
        out = []
        for hit in hits[0:n]:
            context = passages[hit['corpus_id']]
            source = books[context]
            if show:
                print(context, "(Score: {:.4f})".format(hit['cross-score']))
                print(f'From: {source} \n')
            out.append((context,source,hit['cross-score']))    
        return out
    def get_answers(q='A question?', n=3, show=True, bi_encoder=embedder, corpus=text, corpus_embeddings=corpus_embeddings, k=32):
        '''
        Function to get the top n answers for a given question
        Args:
            q (str): Question
            n (int): Number of answers to return
            show (bool): Whether to show the results or not
            bi_encoder (SentenceTransformer or Hugging face file): Sentence embedder
            corpus (list): List of passages
            corpus_embeddings (list): List of embeddings for each passage
            k (int): Number of passages to get from sematnic search
        Returns:
            answers (list): List of top n answers. With each answer as a list [answer, source rulebook, cross encoder score]
        '''
        #sent = get_predictions([q], n=n, show=show)
        answers = search(q, n, show=show,bi_encoder=embedder, passages=corpus, corpus_embeddings=corpus_embeddings, top_k=k)
        #print(answers)
        out = []
        for vals in answers:
            QA_input = {
                'question': q,
                'context': vals[0]
            }
            res = qa_pipeline(QA_input)
            out.append([vals[0],vals[2],res['answer'],vals[1]])
        return out

    out = {}
    for q in qs:
        out[q] = get_answers(q, n=n, show=show)
    return out

def spacy_inference(sentence, model, show=False):
    '''
    Function to use SpaCy to inference SpaCy which has a lot of different information about the words.
    Args:
        sentence (str): Sentence to be infered
        model (SpacyModel): Spacy model
        show (bool): Whether to show the results or not
    Returns:
        doc (Spacy object): Spacy object with a lot of different features.
    '''
    doc = model(sentence)
    if show:
        displacy.render(doc, style='dep', jupyter=True, options={'distance': 120}, )
    return doc

def spacy_get_relation(sentence, model):
    '''
    Function to use spacy to fina a reltion in a sentence
    Args:
        sentence (str): Sentence to be infered
        model (SpacyModel): Spacy model
    Returns:
        span.text (String): Relation
    '''
    doc = model(sentence)

    # Matcher class object 
    matcher = Matcher(model.vocab)

    #define the pattern 
    pattern = [{'DEP':'ROOT'}, 
                {'DEP':'prep','OP':"?"},
                {'DEP':'agent','OP':"?"},  
                {'POS':'ADJ','OP':"?"}] 

    matcher.add("matching_1",[pattern]) 

    matches = matcher(doc)
    k = len(matches) - 1

    span = doc[matches[k][1]:matches[k][2]] 

    return(span.text)

def spacy_get_entity(sentence,model):
    '''
    Function to use spacy to find an entity in a sentence
    Args:
        sentence (str): Sentence to be infered
        model (SpacyModel): Spacy model
    Returns:
        span.text (String): Entity
    '''
  ## chunk 1
    ent1 = ""
    ent2 = ""

    prv_tok_dep = ""    # dependency tag of previous token in the sentence
    prv_tok_text = ""   # previous token in the sentence

    prefix = ""
    modifier = ""

    #############################################################
    
    for tok in model(sentence):
    ## chunk 2
    # if token is a punctuation mark then move on to the next token
        if tok.dep_ != "punct":
        # check: token is a compound word or not
            if tok.dep_ == "compound":
                prefix = tok.text
        # if the previous word was also a 'compound' then add the current word to it
                if prv_tok_dep == "compound":
                    prefix = prv_tok_text + " "+ tok.text
        
        # check: token is a modifier or not
            if tok.dep_.endswith("mod") == True:
                modifier = tok.text
                # if the previous word was also a 'compound' then add the current word to it
                if prv_tok_dep == "compound":
                    modifier = prv_tok_text + " "+ tok.text
        
        ## chunk 3
            if tok.dep_.find("subj") == True:
                ent1 = modifier +" "+ prefix + " "+ tok.text
                prefix = ""
                modifier = ""
                prv_tok_dep = ""
                prv_tok_text = ""      

            ## chunk 4
            if tok.dep_.find("obj") == True:
                ent2 = modifier +" "+ prefix +" "+ tok.text
            
            ## chunk 5  
            # update variables
            prv_tok_dep = tok.dep_
            prv_tok_text = tok.text
        #############################################################

    return [ent1.strip(), ent2.strip()]

def ner_training(model_pth, data_pth, output_folder='../models/new_ner', report_to='none'):
    '''
    Function to train a NER model
    Args:
        model_pth (str): Path to embedder model
        data_pth (str): Path to save the data
        output_folder (str): Path to save the model
        report_to (str): Where to report the results
    '''
    def tokenize_and_align_labels(examples):
        '''
        Function to preprocess the data
        Args:
            examples (transformers dataset): Dataset to be preprocessed
        Returns:
            examples (transformers dataset): Preprocessed dataset
        '''
        tokenized_inputs = tokenizer(examples["tokens"], truncation=True, is_split_into_words=True)
        labels = []

        for i, label in enumerate(examples[f"ner_tags"]):
            word_ids = tokenized_inputs.word_ids(batch_index=i)  # Map tokens to their respective word.
            previous_word_idx = None
            label_ids = []
            for word_idx in word_ids:  # Set the special tokens to -100.
                if word_idx is None:
                    label_ids.append(-100)
                elif word_idx != previous_word_idx:  # Only label the first token of a given word.
                    label_ids.append(label[word_idx])
                else:
                    label_ids.append(-100)
                previous_word_idx = word_idx
            labels.append(label_ids)

        tokenized_inputs["labels"] = labels
        return tokenized_inputs

    metric = load_metric("seqeval")
    def compute_metrics(p):
        # Function to compute different accuracy metrics
        predictions, labels = p
        predictions = np.argmax(predictions, axis=2)
        true_predictions = [
            [label_names[p] for (p, l) in zip(prediction, label) if l != -100]
            for prediction, label in zip(predictions, labels)
        ]
        true_labels = [
            [label_names[l] for (p, l) in zip(prediction, label) if l != -100]
            for prediction, label in zip(predictions, labels)
        ]

        results = metric.compute(predictions=true_predictions, references=true_labels)
        flattened_results = {
            "overall_precision": results["overall_precision"],
            "overall_recall": results["overall_recall"],
            "overall_f1": results["overall_f1"],
            "overall_accuracy": results["overall_accuracy"],
        }
        for k in results.keys():
            if(k not in flattened_results.keys()):
                flattened_results[k+"_f1"]=results[k]["f1"]

        return flattened_results

    train_data = load_dataset('json',data_files=data_pth, split='train[10:]')
    test_data = load_dataset('json',data_files=data_pth, split='train[:10]')
    data = DatasetDict({
        'train': train_data,
        'test': test_data})
    label_names =  ['None','Object','Event','Location', 'Action', 'People', 'Other']
    data["train"].features[f"ner_tags"].feature.names = label_names
    data["test"].features[f"ner_tags"].feature.names = label_names
    tokenizer = AutoTokenizer.from_pretrained(model_pth, truncation=True)
    model = AutoModelForTokenClassification.from_pretrained(model_pth)
    tokenized_data = data.map(tokenize_and_align_labels, batched=True)
    data_collator = DataCollatorForTokenClassification(tokenizer=tokenizer)
    
    training_args = TrainingArguments(
        output_dir=output_folder,
        evaluation_strategy="epoch",
        learning_rate=1e-5,
        per_device_train_batch_size=8,
        per_device_eval_batch_size=8,
        num_train_epochs=40,
        weight_decay=0.01,
        report_to=report_to,
        save_strategy='steps',
        save_steps=200,
    )

    trainer = Trainer(
        model=model,
        args=training_args,
        train_dataset=tokenized_data["train"],
        eval_dataset=tokenized_data["test"],
        tokenizer=tokenizer,
        data_collator=data_collator,
        compute_metrics=compute_metrics
    )

    trainer.train()
