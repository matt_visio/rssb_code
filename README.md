# rssb_code

To get all the neccessary code run this in your terminal/ commnad prompt:

git clone https://gitlab.com/matt_visio/rssb_code

then move into that folder with 

cd rssb_code(this should be the folder name)

Then make sure that you have Anaconda installed. 

To download all the necessary packages run this in your terminal/ anaconda prompt (for windows):

conda env create -f env.yml

Activate the environment with:

conda activate rssb_env

To download the data and models use these commands in your terminal:

data:
gdown --fuzzy "https://drive.google.com/file/d/1BqkKrXhkM59RK6nSWVlu9wSAcamlwOdE/view?usp=sharing"

models:
gdown --fuzzy "https://drive.google.com/file/d/1pFSNPLQK1fvG49JZnpdeuqxf2-QLHLae/view?usp=sharing"

spacy model :
python -m spacy download en_core_web_sm

**or just copy and paste these links into your browser.**

data:
https://drive.google.com/drive/folders/1_SDns2dlpWnVXV_lyI-7NmfOGIjmhvg_?usp=sharing

models:
https://drive.google.com/drive/folders/1f1ARewwHpsUnhmZEP8Mc8jSn1RXCJIdV?usp=sharing


BERTopic colab notebook. Don't download this, just run it on google colab:

https://colab.research.google.com/drive/1cA5q-jIoGwP5pnkWBMlKQqfQDq3iBtfp#scrollTo=A5O3KpHTnVpz

AMR github. The setup to install AMR is on this github and it should be installed in a difference virutal environment: 

https://github.com/ufal/perin
