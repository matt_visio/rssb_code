# %% [markdown]
# # Training the q&a model
# This notebook shows one way to improve the q&a cross-encoder with a user in the loop.
# 
# The method firstly uses the q&a pipeline to get 3 answers for a number of questions.
# 
# Then we loop over the 3 answers and get the user to input which one is the best answer.
# This information is then used to create a dataset where the selected answer is given a strong
# ground truth score of 9 and the other 2 answers are given a score of 6 as they are also relevant
# to the question. The dataset is then used to train the cross encoder model.

# %%
import sys
import pickle
sys.path.insert(1, '../')
from model_calls import qa_inference
from transformers import pipeline
from sentence_transformers import SentenceTransformer, CrossEncoder, InputExample, LoggingHandler
import torch
from torch.utils.data import DataLoader
from sentence_transformers.cross_encoder.evaluation import CERerankingEvaluator
import logging

# %%
### Just some code to print debug information to stdout
logging.basicConfig(format='%(asctime)s - %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    level=logging.INFO,
                    handlers=[LoggingHandler()])
#### /print debug information to stdout

# %% [markdown]
# Loading the data and the models

# %%
data = pickle.load(open("../data/handbooks_parts.pt",'rb'))
data_1 = pickle.load(open("../data/modules_parts.pt",'rb'))
data.extend(data_1)
text = []
from collections import defaultdict
books = defaultdict(list)
for x in data:
    sentence = f"{x['section']}. {x['sentence']}"
    title = f"{x['title']}. {x['sub_title']}"
    if sentence not in text:
        text.append(sentence)
    books[sentence].append(title)

# %%
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
embedder = SentenceTransformer('../models/custom_embedder', device=device)
embeddings = embedder.encode(text, convert_to_tensor=True)
answer_selection_model = "deepset/roberta-base-squad2"
nlp = pipeline('question-answering', model=answer_selection_model, tokenizer=answer_selection_model)
cross_encoder = CrossEncoder('cross-encoder/ms-marco-MiniLM-L-6-v2')

# %% [markdown]
# Running inference on the questions below

# %%
qs = ['How to use communications equipment?', 'What work can be done without a blocked line?',
'What does ABCL mean?','When is a lookout needed?','How to report an accident',
'What does a signaller do?', 'What to do when you see a flood?', 'What does ECO mean?', 
'When are distant lookout allowed?', 'Can you use a mobile phone while acting as a site warden?',
'How must you give the warning as instructed by the COSS or SWL?',
'What must you do when you see the distant lookout giving the warning?', 
'What must you have with you to act as a lookout?',
'Who will make sure you understand the limits of the safe area?',
'How far apart should detonators be placed?',
'Under what conditions should the signaller place detonator protection?',
'Who will make sure you understand the limits of the safe area?',
"What must you do while acting as a site warden?", 'Why must you watch the group?',
'How must you give the warning as instructed by the COSS or SWL?']

# %%
res = qa_inference(embedder=embedder,corpus_embeddings=embeddings, cross_encoder=cross_encoder, qa_pipeline=nlp,text=text, qs=qs, books=books,n=3)

# %% [markdown]
# Loop to get the user to select the best answer for each question and this is used to 
# create the training and development/ validation datasets.

# %%
import json


train = []
dev_samples = {}
i = 0
for q, vals in res.items():
    ids = [0,1,2]
    print()
    print(q)
    for j, v in enumerate(vals):
        print()
        print(f"Answer {j+1}: {v[0]}")
    print()
    index = int(input('Which answer is correct?')) - 1
    answer = vals[index][0]
    ids.remove(index)
    if i < 10:
        example = InputExample(texts=[q, answer], label = 9.0) 
        train.append(example)
        for k in ids:
            example = InputExample(texts=[q, vals[k][0]], label = 6.0)
            train.append(example)
    else:
        qid = i - 10
        dev_samples[qid] = {'query': q, 'positive': set(), 'negative': set()}
        dev_samples[qid]['positive'].add(answer)
        for k in ids:
            dev_samples[qid]['negative'].add(vals[k][0])
    i += 1
    print("\n")

# %% [markdown]
# Training code

# %%
train_dataloader = DataLoader(train, batch_size=1, shuffle=True)
evaluator = CERerankingEvaluator(dev_samples, name='train-eval')
cross_encoder.fit(train_dataloader=train_dataloader,
          loss_fct=torch.nn.MSELoss(),
          evaluator=evaluator,
          epochs=10,
          #evaluation_steps=120,
          #warmup_steps=0,
          output_path='test_qa',
          optimizer_params={'lr':3e-5},
          use_amp=True)
#cross_encoder.save('test_qa')   


